@extends('main')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Stocks</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('productslist') }}">Product list</a></li>
                            <li class="breadcrumb-item active"> product stocks</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    @foreach ($product->stocks as $item)
                    {{-- @php
                             dd($item);
                    @endphp --}}

                    <div class="col-lg-6">
                        <div class="card card-solid">
                            <div class="card-header">
                                <h3 class="card-title">{{$item->warehouse->name}}</h3>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('stocksupdate',$item->id) }}" method="POST">
                                    @csrf
                                <div class="row">

                                        <div class="col-lg-3 col-sm-6 input-group">
                                            <label class="form-label" for="">stock</label>
                                            <input class="form-control " type="text" style="margin: 5px" name="stock"
                                                value="{{$item->stock}}" required>

                                        </div>


                                        <div class="col-lg-3 col-sm-6 input-group">
                                            <label class="form-label" for="">reserved</label>
                                            <input class="form-control" type="text" style="margin: 5px" name="reserved"
                                                value="{{$item->reserved}}" required>

                                        </div>
                                        <div class="col-lg-3 col-sm-6 input-group">
                                            <label class="form-label" for="">reserved back orders</label>
                                            <input class="form-control" type="text" style="margin: 5px" name="reservedbackorders"
                                                value="{{$item->reservedbackorders}}">

                                        </div>

                                        <div class="col-lg-3 col-sm-6 input-group">
                                            <label class="form-label" for="">reserved pick lists</label>
                                            <input class="form-control" type="text" style="margin: 5px" name="reservedpicklists"
                                                value="{{$item->reservedpicklists}}" required>

                                        </div>
                                        <div class="col-lg-3 col-sm-6 input-group">
                                            <label class="form-label" for="">reserved all locations</label>
                                            <input class="form-control" type="text" style="margin: 5px"
                                                name="reservedallocations" value="{{$item->reservedallocations}}">

                                        </div>


                                        <div class="col-lg-3 col-sm-6 input-group">
                                            <label class="form-label" for="">Free Stock</label>
                                            <input class="form-control" type="text" style="margin: 5px" name="freestock"
                                                value="{{$item->freestock}}">

                                        </div>
                                        <div class="col-lg-3 col-sm-6 input-group">
                                            <label class="form-label" for="">Amount</label>
                                            <input class="form-control" type="text" style="margin: 5px" name="amount"
                                                value="{{$item->amount}}">

                                        </div>
                                        <div class="col-lg-3 col-sm-6 input-group">
                                            <label class="form-label" for="">change</label>
                                            <input class="form-control" type="text" style="margin: 5px" name="change"
                                                value="{{$item->change}}">

                                        </div>
                                        <div class="col-lg-3 col-sm-6 input-group">
                                            <label class="form-label" for="">reason</label>
                                            <input class="form-control" type="text" style="margin: 5px" name="reason"
                                                value="{{$item->reason}}">

                                        </div>
                                        <input type="hidden" name="idstock" value="{{$item->id}}">
                                        <input type="hidden" name="idwarehouse" value="{{$item->idwarehouse}}">

                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                        <button class="btn btn-warning" type="reset">reset</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    @endforeach


                </div>

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')

@endsection
