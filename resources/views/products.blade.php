@extends('main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Products</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Products</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Products Table Synchronized with Picqer</h3>
                  <a href="{{route('getproduct')}}" class="btn btn-primary" style="float: right;margin-left: 10px">Synchronize</a>
                  <a href="{{route('productcreate')}}" class="btn btn-info m-10" style="float: right">Create a new product</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>picqer id</th>
                      <th>name</th>
                      <th>price($)</th>
                      <th>fixedstockprice</th>
                      <th>productcode</th>
                      <th>Management</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $item)
                        {{-- @php
                            dd($item->stocks);
                        @endphp --}}
                        <tr>
                            <td>{{$item->idproduct}}</td>
                            <td>{{$item->name}} <a type="button" class="btn btn-sm btn-outline-info" data-toggle="modal" data=""
                                data-target="#myModal{{ $item->id }}"><small>detail</small></a></td>
                            <td>{{$item->price}}</td>
                            <td>{{$item->fixedstockprice}}</td>
                            <td>{{$item->productcode}}</td>
                            <td><a type="button" class="btn btn-info"
                                href="{{route('productedit',$item->id)}}"><i class="fas fa-edit"></i></a>
                            <a type="button" class="btn btn-info"
                                href="{{route('productestocks',$item->id)}}">stock</a>

                            @if ($item->active == 0)
                                <a href="{{route('activeproduct', $item->idproduct)}}"
                                    type="button" class="btn btn-secondary">
                                    <i class="fas fa-toggle-off"></i>
                                </a>
                            @else
                                <a href="{{route('deactiveproduct', $item->idproduct)}}"
                                    type="button" class="btn btn-success">
                                    <i class="fas fa-toggle-on"></i>
                                </a>
                            @endif</td>
                          </tr>
                        @endforeach


                    </tbody>
                    <tfoot>
                    <tr>
                        <th>picqer id</th>
                        <th>name</th>
                        <th>price($)</th>
                        <th>fixedstockprice</th>
                        <th>productcode</th>
                        <th>Management</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
    <!-- /.content -->
  </div>
     {{-- modal : product detail --}}
     @if ($products != null)
     @foreach ($products as $item)
         <div class="modal bd-example-modal-xl" id="myModal{{ $item->id }}" role="dialog">
             <div class="modal-dialog modal-xl" role="document">
                 <div class="modal-content">
                     <div class="card card-solid">
                         <div class="card-header">
                             <h3 class="card-title">{{ $item->name }} Details</h3>
                         </div>
                         <div class="card-body">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">idproduct</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->idproduct}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">idvatgroup</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->idvatgroup}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">price</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->price}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">fixedstockprice</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->fixedstockprice}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">idsupplier</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->idsupplier}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">productcode</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->productcode}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">productcode_supplier</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->productcode_supplier}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">deliverytime</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->deliverytime}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">description</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->description}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">barcode</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->barcode}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">type</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->type}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">unlimitedstock</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->unlimitedstock}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">weight</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->weight}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">length</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->length}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">width</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->name}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">height</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->height}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">minimum_purchase_quantity</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->minimum_purchase_quantity}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">purchase_in_quantities_of</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->purchase_in_quantities_of}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">hs_code</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->hs_code}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">country_of_origin</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->country_of_origin}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">comment_count</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->comment_count}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">analysis_abc_classification</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->analysis_abc_classification}}" >

                                </div>
                                <div class="col-lg-3 col-sm-6 input-group">
                                    <label class="form-label" for="">analysis_pick_amount_per_day</label>
                                    <input class="form-control" type="text"  aria-label="readonly input example" readonly value="{{$item->analysis_pick_amount_per_day}}" >

                                </div>
                                <hr>
                                @foreach ($item->productfield as $field)
                                @php
                                    dd($field);
                                @endphp
                                @endforeach
                            </div>

                         </div>
                         <!-- /.card-body -->
                     </div>
                 </div>
             </div>
         </div>
         </div>
     @endforeach
 @endif
 {{-- modal end --}}
@endsection
@section('js')
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
</script>
@foreach ($products as $item)
    <script>
        $('#myModal{{ $item->id }}').modal(options)
    </script>
    <script>
        const chooseFile = document.getElementById("choose-file");
        const imgPreview{{ $item->id }} = document.getElementById("img-preview{{ $item->id }}");

        chooseFile.addEventListener("change", function() {
            getImgData();
        });

        function getImgData() {
            const files = chooseFile.files[0];
            if (files) {
                const fileReader = new FileReader();
                fileReader.readAsDataURL(files);
                fileReader.addEventListener("load", function() {
                    imgPreview.style.display = "block";
                    imgPreview{{ $item->id }}.innerHTML = '<img src="' + this.result + '" />';
                });
            }
        }
    </script>
@endforeach
@endsection
