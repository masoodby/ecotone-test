@extends('main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-10">
            <h1 class="m-0">Generic Knowledge of Development practices</h1>
            <p>This section contains some questions to asses the knowledge regarding development practices.
            </p>
          </div><!-- /.col -->
          <div class="col-sm-2">
            <ol class="breadcrumb float-sm-right">

              <li class="breadcrumb-item active">Home</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3 style="font-size:1vw;">1. Why is using the FTP protocol a bad practice? </h3>

                <p>Communications and transactions are not encrypted in the FTP protocol. so, if someone has access to the FTP communication route, they can easily listen to the transactions and receive them if necessary.</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-12">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3 style="font-size:1vw;">2. What is the difference between a public and private SSH key?</h3>

                <p>The Public key is available to everyone, but the Private Key must be kept confidential.

                    These two keys are mathematically related to each other, that when a public key is created, a private key is provided at the same time, and when information is encrypted by the public key, it can only be decrypted with the public key.</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-12">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner"><div class="col-12">
                <h3 style="font-size:1vw;">3. You login to a production server and see another developer has left some files in the web root
                    directory (see below), what do you do?</h3>
              </div>

                    <small># /var/www/public_html,
                        .git,
                        app,
                        blog,
                        composer.json,
                        composer.lock,
                        cron.php,
                        production.sql,
                        customers.csv,
                        error_log,
                        errors,
                        index.php,
                        info.php,
                        install.php,
                        js,
                        lib,
                        patch.diff,
                        robots.txt,
                        shell,
                        sitemap.xml,
                        skin,
                        var,
                        vendor</small><hr>
                <p>Copy all the files except the files related to the public_html folder such as (index.php,sitemap,robot,htaccess ,...) in a folder outside the public_html folder, then change the address of the app in the index.php file so that the main folder of the project be secure.</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
            </div>
          </div>
          <!-- ./col -->

          <!-- ./col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
@endsection
@section('js')

@endsection
