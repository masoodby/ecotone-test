@extends('main')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('productslist') }}">Product list</a></li>
                            <li class="breadcrumb-item active">create new product</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="card card-solid">
                        <div class="card-header">
                            <h3 class="card-title">Create a New Product</h3>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('productstore') }}" method="POST">
                                @csrf
                            <div class="row">

                                    <div class="col-lg-3 col-sm-6 input-group">
                                        <label class="form-label" for="">Name</label>
                                        <input class="form-control " type="text" style="margin: 5px" name="name"
                                            value="{{ old('name') }}" required>

                                    </div>


                                    <div class="col-lg-3 col-sm-6 input-group">
                                        <label class="form-label" for="">price</label>
                                        <input class="form-control" type="text" style="margin: 5px" name="price"
                                            value="{{ old('price') }}" required>

                                    </div>
                                    <div class="col-lg-3 col-sm-6 input-group">
                                        <label class="form-label" for="">fixedstockprice</label>
                                        <input class="form-control" type="text" style="margin: 5px" name="fixedstockprice"
                                            value="{{ old('fixedstockprice') }}">

                                    </div>

                                    <div class="col-lg-3 col-sm-6 input-group">
                                        <label class="form-label" for="">productcode</label>
                                        <input class="form-control" type="text" style="margin: 5px" name="productcode"
                                            value="{{ old('productcode') }}" required>

                                    </div>
                                    <div class="col-lg-3 col-sm-6 input-group">
                                        <label class="form-label" for="">productcode_supplier</label>
                                        <input class="form-control" type="text" style="margin: 5px"
                                            name="productcode_supplier" value="{{ old('productcode_supplier') }}">

                                    </div>


                                    <div class="col-lg-3 col-sm-6 input-group">
                                        <label class="form-label" for="">barcode</label>
                                        <input class="form-control" type="text" style="margin: 5px" name="barcode"
                                            value="{{ old('barcode') }}">

                                    </div>
                                    <div class="col-lg-3 col-sm-6 input-group">
                                        <label class="form-label" for="">weight</label>
                                        <input class="form-control" type="text" style="margin: 5px" name="weight"
                                            value="{{ old('weight') }}">

                                    </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                    <button class="btn btn-warning" type="reset">reset</button>
                                </div>
                            </div>
                        </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')

@endsection
