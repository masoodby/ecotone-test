@extends('main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">{{$warehouse->name}} Stock list</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{route('warehouseslist')}}">Warehouses</a></li>
              <li class="breadcrumb-item active">Stocks List</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>warehouse id</th>
                      <th>product name</th>
                      <th>stock</th>
                      <th>reserved</th>
                      <th>reserved back orders</th>
                      <th>reserved pick lists</th>
                      <th>reserve dall locations</th>
                      <th>free stock</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($warehouse->stocks as $item)
                        {{-- @php
                            dd($item);
                        @endphp --}}
                        <tr>
                            <td>{{$item->idwarehouse}}</td>
                            <td>{{$item->products->name}}</td>
                            <td>{{$item->stock}}</td>
                            <td>{{$item->reserved}}</td>
                            <td>{{$item->reservedbackorders}}</td>
                            <td>{{$item->reservedallocations }}</td>
                            <td>{{$item->freestock }}</td>
                            <td></tr>
                        @endforeach


                    </tbody>
                    <tfoot>
                    <tr>
                        <th>warehouse id</th>
                        <th>product name</th>
                        <th>stock</th>
                        <th>reserved</th>
                        <th>reserved back orders</th>
                        <th>reserved pick lists</th>
                        <th>reserve dall locations</th>
                        <th>free stock</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
    <!-- /.content -->
  </div>
     {{-- modal : product detail --}}

 {{-- modal end --}}
@endsection
@section('js')
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
</script>

@endsection
