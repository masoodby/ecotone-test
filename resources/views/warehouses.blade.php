@extends('main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Warehouses</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Products</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Warehouses Table Synchronized with Picqer</h3>
                  <a href="{{route('warehouses')}}" class="btn btn-primary" style="float: right;margin-left: 10px">Synchronize</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>picqer id</th>
                      <th>name</th>
                      <th>accept orders</th>
                      <th>counts for general stock</th>
                      <th>priority</th>
                      <th>Status</th>
                      <th>Management</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($warehouses as $item)
                        {{-- @php
                            dd($item->stocks);
                        @endphp --}}
                        <tr>
                            <td>{{$item->idwarehouse}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->accept_orders}}</td>
                            <td>{{$item->counts_for_general_stock}}</td>
                            <td>{{$item->priority}}</td>
                            <td>{{$item->active == 1 ? 'Active' :'InActive' }}</td>


                                <td><a type="button" class="btn btn-info" href="{{route('stocklist',$item->id)}}">stocks</a>
                                <a type="button" class="btn btn-info" href="{{route('locationslist',$item->id)}}">Locations</a></td>


                          </tr>
                        @endforeach


                    </tbody>
                    <tfoot>
                    <tr>
                        <th>picqer id</th>
                      <th>name</th>
                      <th>accept orders</th>
                      <th>counts for general stock</th>
                      <th>priority</th>
                      <th>Status</th>
                      <th>Management</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
    <!-- /.content -->
  </div>
     {{-- modal : product detail --}}

 {{-- modal end --}}
@endsection
@section('js')
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
</script>
@foreach ($warehouses as $item)
    <script>
        $('#myModal{{ $item->id }}').modal(options)
    </script>
    <script>
        const chooseFile = document.getElementById("choose-file");
        const imgPreview{{ $item->id }} = document.getElementById("img-preview{{ $item->id }}");

        chooseFile.addEventListener("change", function() {
            getImgData();
        });

        function getImgData() {
            const files = chooseFile.files[0];
            if (files) {
                const fileReader = new FileReader();
                fileReader.readAsDataURL(files);
                fileReader.addEventListener("load", function() {
                    imgPreview.style.display = "block";
                    imgPreview{{ $item->id }}.innerHTML = '<img src="' + this.result + '" />';
                });
            }
        }
    </script>
@endforeach
@endsection
