<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('idwarehouse');
            $table->foreign('idwarehouse')->references('idwarehouse')->on('warehouses')->onDelete('cascade');
            $table->integer('idlocation');
            $table->string('name');
            $table->string('remarks')->nullable();
            $table->boolean('unlink_on_empty');
            $table->json('location_type');
            $table->boolean('is_bulk_location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
