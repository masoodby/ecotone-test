<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductfieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productfields', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('idproduct');
            $table->foreign('idproduct')->references('idproduct')->on('products')->onDelete('cascade');
            $table->unsignedInteger('idproductfield');
            $table->string('title');
            $table->text('type');
            $table->json('value')->nullable();
            $table->boolean('required')->default(0);
            $table->boolean('visible_picklist')->default(0);
            $table->boolean('visible_invoice')->default(0);
            $table->boolean('visible_shippinglist')->default(0);
            $table->boolean('visible_portal')->default(0);
            $table->boolean('visible_purchase_order')->default(0);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productfields');
    }
}
