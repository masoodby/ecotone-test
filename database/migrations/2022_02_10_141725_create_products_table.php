<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('idproduct')->unique()->unsigned()->index();
            $table->unsignedInteger('idvatgroup');
            $table->string('name');
            $table->float('price');
            $table->float('fixedstockprice')->default(0);
            $table->unsignedInteger('idsupplier')->nullable();
            $table->string('productcode');
            $table->string('productcode_supplier')->nullable();
            $table->integer('deliverytime')->nullable();
            $table->string('description')->nullable();
            $table->string('barcode')->nullable();
            $table->string('type')->nullable();
            $table->boolean('unlimitedstock')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('length')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->integer('minimum_purchase_quantity')->default(0);
            $table->integer('purchase_in_quantities_of')->default(0);
            $table->string('hs_code')->nullable();
            $table->string('country_of_origin')->nullable();
            $table->boolean('active')->nullable();
            $table->integer('comment_count')->default(0);
            $table->boolean('analysis_abc_classification')->nullable();
            $table->boolean('analysis_pick_amount_per_day')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
