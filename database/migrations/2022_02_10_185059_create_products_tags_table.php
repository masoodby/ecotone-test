<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_tags', function (Blueprint $table) {
            $table->unsignedInteger('idtag');
            $table->foreign('idtag')->references('idtag')->on('tags')->onDelete('cascade');
            $table->unsignedInteger('idproduct');
            $table->foreign('idproduct')->references('idproduct')->on('products')->onDelete('cascade');
            $table->primary(['idtag' , 'idproduct']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_tags');
    }
}
