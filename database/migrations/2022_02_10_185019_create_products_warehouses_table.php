<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_warehouses', function (Blueprint $table) {
            $table->unsignedInteger('idwarehouse');
            $table->foreign('idwarehouse')->references('idwarehouse')->on('warehouses')->onDelete('cascade');
            $table->unsignedInteger('idproduct');
            $table->foreign('idproduct')->references('idproduct')->on('products')->onDelete('cascade');
            $table->primary(['idwarehouse' , 'idproduct']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_warehouses');
    }
}
