<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('idwarehouse');
            $table->foreign('idwarehouse')->references('idwarehouse')->on('warehouses')->onDelete('cascade');
            $table->unsignedInteger('idproduct');
            $table->foreign('idproduct')->references('idproduct')->on('products')->onDelete('cascade');
            $table->integer('stock')->unsigned();
            $table->boolean('reserved');
            $table->boolean('reservedbackorders');
            $table->boolean('reservedpicklists');
            $table->boolean('reservedallocations');

            $table->integer('freestock')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
