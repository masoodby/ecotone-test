<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Warehouse;
use App\Models\Stock;
use App\Models\Productfiled;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::orderbydesc('idproduct')->with('stocks','productfield')->get();
        // dd($products);
        return view('products',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([


            'productcode'=> 'required',
            'productcode_supplier'=> 'required',
            'name' => 'required',
            'price' => 'required',
            'fixedstockprice' => 'required',
            'weight' => 'required',
            'barcode' => 'required',

        ]);

        $picqr = new PicqerController;
        $product= $picqr->NewProduct($request);
        // productstore
        $picqr->GetAllProducts();
        return redirect(route('productslist'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {


        return view('productEdit',compact('product'));
    }
    public function stocks(Product $product)
    {
        // dd($product->stocks);
        return view('productStock',compact('product'));
    }
    public function stocksupdate(Product $product,Request $request)
    {
        // dd($request->all());

        $picqr = new PicqerController;
       if($request->amount){
        $warehouses= $picqr->stockWarehouse($product->idproduct, $request->idwarehouse, [
            "reason" => $request['reason'],
            "amount" => $request['amount'],

        ]);
       }else{
        $warehouses= $picqr->stockWarehouse($product->idproduct, $request->idwarehouse, [
            "reason" => $request['reason'],
            "change"  => $request['change'],


        ]);
       }
       $stock=Stock::where('id', $request->idstock )->first();
        $stock->update([
            "stock" => $warehouses['data']['stock'],
            "reserved"  => $warehouses['data']['reserved'],
            "reservedbackorders" => $warehouses['data']['reservedbackorders'],
            "reservedpicklists"  => $warehouses['data']['reservedpicklists'],
            "reservedallocations" => $warehouses['data']['reservedallocations'],
            "freestock"  => $warehouses['data']['freestock'],
        ]);

        return redirect(route('productslist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Product $product,Request $request)
    {
        //updateProduct
        $product->update([
            'idproduct' => $product->idproduct,
            'productcode'=> $request['productcode'],
            'productcode_supplier'=> $request['productcode_supplier'],
            'name' =>$request['name'],
            'price' => $request['price'],
            'fixedstockprice' => $request['fixedstockprice'],
            'weight' => $request['weight'],
            'barcode' => $request['barcode'],
        ]);

        $picqr = new PicqerController;
        $product= $picqr->updateProduct($product->idproduct,[
            'productcode'=> $request['productcode'],
            'productcode_supplier'=> $request['productcode_supplier'],
            'name' =>$request['name'],
            'price' => $request['price'],
            'fixedstockprice' => $request['fixedstockprice'],
            'weight' => $request['weight'],
            'barcode' => $request['barcode'],
        ]);
        // $picqr->GetAllProducts();
        return redirect(route('productslist'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function activeProduct($idproduct){
        $picqr = new PicqerController;
       $active= $picqr->Active($idproduct);

       $product=Product::where('idproduct',$idproduct)->first();
       $product->update(['active' => 1]);


        return redirect(route('productslist'));
    }
    public function deactiveProduct($idproduct){
        $picqr = new PicqerController;
        $result= $picqr->updateProduct($idproduct,['stock'=>[]]);

        $result= $picqr->inActive($idproduct);


          $product=Product::where('idproduct',$idproduct)->first();
          $product->update(['active' => 0]);
        //   dd($product);
        return redirect(route('productslist'));
    }
    public function getlocations($idproduct){
        $picqr = new PicqerController;
        $result= $picqr->updateProduct($idproduct,['stock'=>[]]);

        $result= $picqr->inActive($idproduct);


          $product=Product::where('idproduct',$idproduct)->first();
          $product->update(['active' => 0]);
        //   dd($product);
        return redirect(route('productslist'));
    }

}
