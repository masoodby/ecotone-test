<?php

namespace App\Http\Controllers;

use App\Models\Location;

use App\Http\Controllers\PicqerController;
class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations=Location::orderbydesc('idlocation')->with('warehouses')->get();
        return view('location',compact('locations'));
    }
    public function getlocations()
    {
        $picqr = new PicqerController;
        $locations= $picqr->getlocations();
        // dd($locations);
        foreach($locations['data'] as $location){
            // dd($location);
            Location::updateOrCreate(
            [
                "idlocation" => $location["idlocation"],
                "idwarehouse" => $location["idwarehouse"],
            ],[

                "parent_idlocation"=> $location["parent_idlocation"],
                "name" => $location["name"],
                "remarks"=> $location["remarks"],
                "unlink_on_empty" => $location["unlink_on_empty"],
                "location_type"  => $location["location_type"],
                "is_bulk_location"  => $location["is_bulk_location"],
            ]);
        }

        return redirect(route('productslist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLocationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLocationRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLocationRequest  $request
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLocationRequest $request, Location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        //
    }
}
