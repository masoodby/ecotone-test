<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Productfiled;
use App\Models\Stock;
use App\Models\Warehouse;
use Illuminate\Http\Request;

class PicqerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    private $key= 'I2Tc124Kp0qBvI0glLMUi6souHsMimyh8C2fOk8hk59Rf3qg';
    private $subdomain='fairweb';
    public function GetAllProducts()
    {
        //    get and fetch and update the local DB
        $this->getallWarehouses();
        $apiClient = new \Picqer\Api\Client($this->subdomain , $this->key );

        $apiClient->enableRetryOnRateLimitHit();

        $apiClient->setUseragent('My amazing app (dev@example.org)');

        // Retrieve all products from Picqer account
        $products = $apiClient->getProducts();
        // dd($products);
        foreach($products['data'] as $product){
            // dd($product);


          Product::updateOrCreate(
                [
                'idproduct' => $product['idproduct'],
                ],
                [
                    'idproduct'=> $product['idproduct'],
                    'idvatgroup'=> $product['idvatgroup'],
                    'name'=> $product['name'],
                    'price'=> $product['price'],
                    'fixedstockprice'=> $product['fixedstockprice'],
                    'idsupplier'=> $product['idsupplier'],
                    'productcode'=> $product['productcode'],
                    'productcode_supplier'=> $product['productcode_supplier'],
                    'deliverytime'=> $product['deliverytime'],
                    'description'=> $product['description'],
                    'barcode'=> $product['barcode'],
                    'type'=> $product['type'],
                    'weight'=> $product['weight'],
                    'length'=> $product['length'],
                    'height'=> $product['height'],
                    'minimum_purchase_quantity'=> $product['minimum_purchase_quantity'],
                    'purchase_in_quantities_of'=> $product['purchase_in_quantities_of'],
                    'hs_code'=> $product['hs_code'],
                    'country_of_origin'=> $product['country_of_origin'],
                    'active'=> $product['active'],
                    'unlimitedstock'=> $product['unlimitedstock'],
                    // 'idfulfilment_customer'=> $product['idfulfilment_customer'],
                ]);
                foreach ($product['stock'] as  $value) {

                   Stock::updateOrCreate([
                    "idwarehouse" => $value['idwarehouse'],
                    "idproduct" => $product['idproduct'],
                   ],[

                    "stock" => $value['stock'],
                    "reserved" => $value['reserved'],
                    "reservedbackorders" => $value['reservedbackorders'],
                    "reservedpicklists" => $value['reservedpicklists'],
                    "reservedallocations" => $value['reservedallocations'],
                    "freestock" => $value['freestock'],
                   ]);
                }
                foreach($product['productfields'] as  $value){
                    Productfiled::updateOrCreate([
                        "idproductfield" => $value['idproductfield'],
                        "idproduct" => $value['idproduct'],
                    ],[

                        "title" => $value['title'],
                        "type" => $value['type'],
                        "value" => $value['value'],
                        "required" => $value['required'],
                        "visible_picklist" => $value['visible_picklist'],
                        "visible_invoice" => $value['visible_invoice'],
                        "visible_shippinglist" => $value['visible_shippinglist'],
                        "visible_portal" => $value['visible_portal'],
                        "visible_purchase_order" => $value['visible_purchase_order'],
                    ]);
                }
        }

        return redirect(route('productslist'));
    }
    public function NewProduct(Request $request)
    {
        //    add products
        $apiClient = new \Picqer\Api\Client($this->subdomain , $this->key );
        $apiClient->enableRetryOnRateLimitHit();
        $apiClient->setUseragent('My amazing app (dev@example.org)');

        // Retrieve VAT groups
        $vatGroups = $apiClient->getVatgroups();

        // Add a new product to Picqer account
        $product = [
            'productcode' => $request['productcode'],
            'productcode_supplier' => $request['productcode_supplier'],
            'name' => $request['name'],
            'price' => $request['price'],
            'fixedstockprice' => $request['fixedstockprice'],
            'weight' => $request['weight'],
            'barcode' => $request['barcode'],
            'idvatgroup' => $vatGroups['data'][0]['idvatgroup'] // First VAT group in Picqer
        ];

        $result = $apiClient->addProduct($product);
        // var_dump($result);
        return $result;
    }
    public function UpdateProduct($idproduct, $params)
    {
        $apiClient = new \Picqer\Api\Client($this->subdomain , $this->key );

        $apiClient->enableRetryOnRateLimitHit();

        $apiClient->setUseragent('My amazing app (dev@example.org)');

        // Retrieve all products from Picqer account
        $result = $apiClient->updateProduct($idproduct , $params);

        //    get and fetch and update the local DB
        return true;
    }
    public function inActive($idproduct)
    {
        $apiClient = new \Picqer\Api\Client($this->subdomain , $this->key );

        $apiClient->enableRetryOnRateLimitHit();

        $apiClient->setUseragent('My amazing app (dev@example.org)');

        // Retrieve all products from Picqer account
        $result = $apiClient->inactivateProduct($idproduct);
        return $result;
    }
    public function Active($idproduct)
    {
        $apiClient = new \Picqer\Api\Client($this->subdomain , $this->key );

          $apiClient->enableRetryOnRateLimitHit();

          $apiClient->setUseragent('My amazing app (dev@example.org)');

          // Retrieve all products from Picqer account
          $result = $apiClient->activateProduct($idproduct);
        return $result;
    }

    public function getallWarehouses(){
          //    get and fetch and update the local DB

          $apiClient = new \Picqer\Api\Client($this->subdomain , $this->key );

          $apiClient->enableRetryOnRateLimitHit();

          $apiClient->setUseragent('My amazing app (dev@example.org)');

          // Retrieve all products from Picqer account
          $warehouses = $apiClient->getWarehouses();
        //   dd($warehouses);
          foreach($warehouses['data'] as $warehouse){
            //   dd($warehouse);
              if($warehouse['accept_orders'] == true){
                $accept_orders =  1;
              }else{
                  $accept_orders =  0;
              };
              if($warehouse['counts_for_general_stock'] == true){
                $counts_for_general_stock =  1;
              }else{
                  $counts_for_general_stock =  0;
              };

            Warehouse::updateOrCreate(
                  [
                  'idwarehouse' => $warehouse['idwarehouse'],
                  ],
                  [
                    "idwarehouse" => $warehouse['idwarehouse'],
                    "name" => $warehouse['name'],
                    "accept_orders" => $accept_orders,
                    "counts_for_general_stock" => $counts_for_general_stock,
                    "priority" => $warehouse['priority'],
                    "active" => $warehouse['active'],
                  ]
                  );
          }

          return redirect(route('warehouseslist'));
    }
    public function stockWarehouse($idproduct, $idwarehouse, $params){
          //    get and fetch and update the local DB

          $apiClient = new \Picqer\Api\Client($this->subdomain , $this->key );

          $apiClient->enableRetryOnRateLimitHit();

          $apiClient->setUseragent('My amazing app (dev@example.org)');

          // Retrieve all products from Picqer account
          $warehouses = $apiClient->updateProductStockForWarehouse($idproduct, $idwarehouse, $params);
        //    dd($warehouses);


          return $warehouses;
    }
    public function getlocations(){
          //    get and fetch and update the local DB

          $apiClient = new \Picqer\Api\Client($this->subdomain , $this->key );

          $apiClient->enableRetryOnRateLimitHit();

          $apiClient->setUseragent('My amazing app (dev@example.org)');

          // Retrieve all products from Picqer account
          $locations = $apiClient->getLocations();
        //  dd($locations);


          return $locations;
    }

}
