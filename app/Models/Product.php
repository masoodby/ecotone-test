<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'idproduct',
        'idvatgroup',
        'name',
        'price',
        'fixedstockprice',
        'idsupplier',
        'productcode',
        'productcode_supplier',
        'deliverytime',
        'description',
        'barcode',
        'unlimitedstock',
        'type',
        'weight',
        'length',
        'height',
        'minimum_purchase_quantity',
        'purchase_in_quantities_of',
        'hs_code',
        'country_of_origin',
        'active',


    ];
    public function stocks(){
        return $this->hasMany(Stock::class ,'idproduct','idproduct');
    }
    public function productfield(){
        return $this->hasMany(Productfiled::class ,'idproduct','idproduct');
    }
}
