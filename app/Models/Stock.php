<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table= "stocks";
    protected $guarded = [];
    public function warehouse(){
        return $this->hasone(warehouse::class, 'idwarehouse','idwarehouse');
    }
    public function products(){
        return $this->hasone(Product::class, 'idproduct','idproduct');
    }
}
