<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $table= "warehouses";
    protected $guarded = [];

    public function stocks(){
        return $this->hasMany(Stock::class ,'idwarehouse','idwarehouse');
    }
    public function locations(){
        return $this->hasMany(Location::class ,'idwarehouse','idwarehouse');
    }
}
