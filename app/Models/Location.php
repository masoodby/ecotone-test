<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table= "locations";
    protected $guarded = [];
    protected $casts = [
        'location_type' => 'array'
    ];
  public function parent(){
      return $this->belongsTo(Location::class,'parent_idlocation');
  }
}
