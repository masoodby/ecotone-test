<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('main');
// });
Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/productslist', [App\Http\Controllers\ProductController::class, 'index'])->name('productslist');
Route::get('/product-create', [App\Http\Controllers\ProductController::class, 'create'])->name('productcreate');
Route::post('/product-create', [App\Http\Controllers\ProductController::class, 'store'])->name('productstore');
Route::get('/product-stocks/{product}', [App\Http\Controllers\ProductController::class, 'stocks'])->name('productestocks');
Route::post('/product-stocks/{product}', [App\Http\Controllers\ProductController::class, 'stocksupdate'])->name('stocksupdate');
Route::get('/product-edit/{product}', [App\Http\Controllers\ProductController::class, 'edit'])->name('productedit');
Route::post('/product-update/{product}', [App\Http\Controllers\ProductController::class, 'update'])->name('productupdate');
Route::get('/product-active/{idproduct}', [App\Http\Controllers\ProductController::class, 'activeProduct'])->name('activeproduct');
Route::get('/product-deactive/{idproduct}', [App\Http\Controllers\ProductController::class, 'deactiveProduct'])->name('deactiveproduct');
Route::get('/products', [App\Http\Controllers\PicqerController::class, 'GetAllProducts'])->name('getproduct');


///////////////////warehouses////////////////////
Route::get('/warehouses', [App\Http\Controllers\PicqerController::class, 'getallWarehouses'])->name('warehouses');
Route::get('/warehouses-list', [App\Http\Controllers\WarehouseController::class, 'index'])->name('warehouseslist');
Route::get('/warehouses-stocks-list/{warehouse}', [App\Http\Controllers\WarehouseController::class, 'stocklist'])->name('stocklist');
Route::get('/warehouses-locations-list/{warehouse}', [App\Http\Controllers\WarehouseController::class, 'locations'])->name('locationslist');


///////////////tags//////////////////////
Route::get('/tags', [App\Http\Controllers\PicqerController::class, 'alltags'])->name('tags');

/////////////////////locations/////////////////////

Route::get('/locations-list', [App\Http\Controllers\LocationController::class, 'index'])->name('locationlistlist');
Route::get('/get-locations-list', [App\Http\Controllers\LocationController::class, 'getlocations'])->name('getlocations');
